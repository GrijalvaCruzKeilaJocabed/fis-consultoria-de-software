## PRIMER MAPA CONCEPTUAL: LA CONSULTORIA
### Recurso: https://canal.uned.es/video/5a6f887cb1111f81638b4570
```plantuml
@startmindmap
*[#LightBlue] **La consultoria**
	*[#Yellow] **Definicion** 
		*[#Orange] **Empresa de servicios profesionales** \n **de alto valor**
		*[#Orange] **Modo de vida para alcanzar la felicidad**
	*[#Yellow] **Nacen** 
		*[#LightGreen] **Existen organizaciones con complejidad ** \n **y que requieren informacion**
		*[#LightGreen] **No pueden operar y transformarse**
	*[#Yellow] **Tiene 3 caracteristicas** 
		*[#Violet] **Conocimiento sectorial**
		*[#Violet] **Conocimiento tecnico**
		*[#Violet] **Tener capacidad de plantear soluciones**
	*[#Yellow] **Ejemplo de una consultoria**
		*[#LightSalmon] **AXPE**
			*_ ¿Que es?
				*[#LightSteelBlue] **Compañia multinacional de consultoria** \n **y tecnologias de informacion**
			*_ Se especializa
				*[#LightSteelBlue] **Servicios de Outsorcing**
				*[#LightSteelBlue] **Consultoria**
				*[#LightSteelBlue] **Ejecucion de proyectos cerrados**
	*[#Yellow] **¿Que tipo de servicio presta?**
		*[#LightCyan] **Consultoria**
			*_ Reingenieria de procesos
				*[#Green] **Como ayudar a las empresas** \n **a organizar actividades**
					*_ siguiendo
						*[#LightGreen] **un esquema**
			*_ Gobierno TI
				*[#Green] **Dirigida al responsable ** \n **de la organizacion**
				*[#Green] **Las mejores practicas**
					*_ son:
						*[#LightGreen] **Calidad**
						*[#LightGreen] **Metodologia de desarrollo**
						*[#LightGreen] **Arq. Empresarial**                                                
			*_ Oficina de proyectos
				*[#LightGreen] **Se gestione correctamente los proyectos**
				*[#LightGreen] **Estandares de referencia**
			*_ Analitica avanzada de datos
				*_ Se debe saber
					*[#Green] **La informacion es crucial**
						*_ Para 
							*[#LightGreen] **la empresa**
							*[#LightGreen] **Informacion en tiempo real**
							*[#LightGreen] **Hoy se genera muchisima informacion**
				*_ Obtener conocimientos mediante:
					*[#Green] **Aplicacion de actividades** \n **de machine learning**
					*[#Green] **Tecnologias de base (Big Date)**        
		*[#LightCyan] **Integracion**
			*[#Pink] **Toda empresa tiene un**
				*_ Mapa 
					*[#LightPink] **Piezas de software**
					*[#LightPink] **Piezas de Hadware**
					*_ requiere    
						*[#LightPink] **Evaluacion continua**
			*_ Utilizacion de
				*[#Pink] **Desarrollos a medida**
					*[#LightPink] **Diferentes lenguajes y tecnologias**
				*[#Pink] **Aseguramiento calidad de software**
					*[#LightPink] **Negocios extremo a extremo**
					*[#LightPink] **Asegurar un proceso de negocio**
				*[#Pink] **Infraestructuras**
					*[#LightPink] **Definicion del uso de tecnologias**
					*[#LightPink] **Plataformas**
					*[#LightPink] **Migrar**
					*[#LightPink] **Operativizar**
				*[#Pink] **Soluciones de mercado** 
					*[#LightPink] **Trasladar a los sistemas ya construidos**
					*[#LightPink] **Productos como:**
						*[#LightPink] **SAP**
						*[#LightPink] **SalesForce**
		*[#LightCyan] **Externalizacion**
			*[#Blue] **Definir organizaciones se servicios** \n **a largo plazo**
			*[#Blue] **Consultas asumen actividades**
				*[#LightBlue] **Eficiencia**
				*[#LightBlue] **Eficacia**
			*[#Blue] **Ambitos**
				*_ Gestion de aplicaciones
					*[#LightBlue] **Empresas delegan ** \n **mantenimiento de sistema**
					*[#LightBlue] **Evitar la gestion** \n **interna de recursos**
				*_ Servicios SQA
				*_ Operacion y administracion de infraestructura
					*[#LightBlue] **Tipo de Outsorcing**
					*[#LightBlue] **Area de crecimiento importante**
					*[#LightBlue] **BPO**
				*_ Procesos de negocios
@endmindmap
```mindmap
```
## CONSULTORIA DE SOFTWARE
### Recursos:
https://canal.uned.es/video/5a6f1733b1111f35718b4580

https://canal.uned.es/video/5a6f1734b1111f35718b4586

https://canal.uned.es/video/5a6f1730b1111f35718b456a

https://canal.uned.es/video/5a6f1734b1111f35718b458b
```plantuml
@startmindmap
*[#Violet] **Consultoria de Software**
	*[#Blue] **Desarrollo de Software**
		*_ Es un 
			*[#LightBlue] **Es un proceso largo**
		*_ Comienza
			*[#LightBlue] **Cuando el cliente**
				*_ tiene
					*[#LightBlue] **una necesidad en su negocio**
		*_ Primer Paso
			*_ Hacer consultoria
				*[#LightBlue] **Hablar**
				*[#LightBlue] **Requisitos**
			*_ Estudio de viabilidad
				*[#LightBlue] **Analizar lo que quiere**
				*[#LightBlue] **Ventajas**
				*[#LightBlue] **Ahorro**
				*[#LightBlue] **Costo**
				*[#LightBlue] **Ver si es viable**
			*_ Diseño funcional
				*[#LightBlue] **Informacion de entrada**
				*[#LightBlue] **Informacion de salida**
				*[#LightBlue] **Almacenar en una BD**
				*[#LightBlue] **Construir modelo de datos**
				*[#LightBlue] **Contar los procesos**
				*[#LightBlue] **Realizar prototipos**
		*_ Segundo Paso
			*[#LightBlue] **Pruebas funcionales**
				*[#LightBlue] ** Tecnicos **
				*[#LightBlue] ** Funcionales **
			*[#LightBlue] **Realizar pruebas de usuario**
				*[#LightBlue] **Implantar en todos** \n **los servidores**
				*[#LightBlue] **Subir a internet**
			*[#LightBlue] **Relacionarse con los clientes**
				*_ Lo realizan mas 
					*[#LightBlue] **Los jefes de proyecto**
				*[#LightBlue] **Solo en un 5% a 10% del proceso**
				*[#LightBlue] **Entran las metodologias de gestion**
		*_ Diseño tecnico
			*[#LightBlue] **Aislar tipos de negocio**
			*[#LightBlue] **Diseñadpr trasladar con lenguaje de programacion**
			*[#LightBlue] **Entorno de Hardware**
			*[#LightBlue] **Tipo de BD**
			*[#LightBlue] **Programadores**
				*_ Tarea:
					*[#LightBlue] **Crear procesos y necesidades de datos**
					*[#LightBlue] **Construir el software**
					*[#LightBlue] **Realiza pruebas internas **
					*[#LightBlue] **Pruebas integradas** 
	*_ Actividades desarrolladas en AXPE
		*_ Tareas como..
			*[#Orange] **Tareas que permiten crear** \n **o mantener la informatica de la empresa**
			*[#Orange] **Servicios a empresa**
			*[#Orange] **Empresa necesita**
				*[#OrangeRed] **Infraestructura**
				*[#OrangeRed] **Internet**
				*[#OrangeRed] **Aplicaciones**
					*_ [Software]
			*[#Orange] **Infraestructura siempre funciona**
			*[#Orange] **Cambios se adecuen**
			*[#Orange] **No tengan problemas**
		*_ Factores
			*_ Soluciones software
				*[#OrangeRed] **Especificar**
				*[#OrangeRed] **Diseñar**
				*[#OrangeRed] **Desarrollar Soluciones**
			*_ Metodologia
				*_ se basa
					*[#OrangeRed] **Metrica 3**
					*[#OrangeRed] **CMMI**
			*_ Lineas de produccion
				*[#OrangeRed] **COBOL**
				*[#OrangeRed] **.net**
				*[#OrangeRed] **COM**
				*[#OrangeRed] **Sharepoint**
	*_ Tendencias en el desarrollo de servicios
		*[#Green] **Cambiar el concepto de Informacion**
		*[#Green] **Informacion**
			*_ como ..
				*[#LightGreen] **En la nube**
				*[#LightGreen] **Google**
				*[#LightGreen] **Amazon**
				*[#LightGreen] **Empresas especializadas**
		*[#Green] **Nivel de datos**
			*_ Almacenan 
				*[#LightGreen] **Facebook**
				*[#LightGreen] **Youtube**
				*[#LightGreen] **Twitter**
				*[#LightGreen] **WhatsApp**
		*[#Green] **Informacion Valida**
			*_ Cambiando forma de procesar
				*[#LightGreen] **Estadistico**
				*[#LightGreen] **Matematicas**
		*[#Green] **Inconvenientes**
			*[#LightGreen] **Cliente no quiere informacion** \n ** a manos de un tecnico**
			*[#LightGreen] **Problemas con el correo**
			*[#LightGreen] **Leyes con Google**
			*[#LightGreen] **Seguridad de informacion**
@endmindmap
```mindmap
```
## Aplicacion de la Ingenieria de Software
### Recurso: https://canal.uned.es/video/5a6f4c4eb1111f082a8b4978
```plantuml
@startmindmap
*[#Violet] ** Aplicacion de Ingenieria de Software**
	*[#Orange] **Modelo de Aplicacion**
		*[#Blue] **Stratesys**
			*_ ¿Que es?
				*[#LightBlue] **Primera empresa de** \n **software de gestion**
				*[#LightBlue] **Paquete de software** \n **configurable**  
			*_ Señas de identidad
				*[#LightBlue] **Innovacion**
				*[#LightBlue] **Conocimiento**
				*[#LightBlue] **Especializacion**
				*[#LightBlue] **Calidad**
	*[#Orange] **¿Que trae?**
		*[#LightPink] **Bases practicas**
			*[#LightPink] **Procesos de negocio**
		*[#LightPink] **Framework de desarrollo**
			*[#LightPink] **Desarrollo en lenguaje ABA**
	*[#Orange] **Aplicacion**
		*[#LightGreen] **Fases**
			*[#LightGreen] **Recogida de requisitos**
			*[#LightGreen] **Diseño funcional**
			*[#LightGreen] **Pruebas**
	*[#Orange] **Herramientas de gestion**
		*[#IndianRed] **Biblioteca template**
	*[#Orange] **Procesos**
		*[#Orange] **Enfoque desarrollo**
		*[#Orange] **Vision global**
		*[#Orange] **Detalle proceso**
		*[#Orange] **Analisis funcional**
		*[#Orange] **Cliente prueba**
		*[#Orange] **Analisis Tecnico**
@endmindmap
```mindmap

